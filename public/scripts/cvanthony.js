// Obtenir le modal
var modal = document.getElementById('mymodal');

// Créer le btn qui ouvre le modal
var btn = document.getElementById("infobtn");

// Créer le <span> qui ferme le modal
var span = document.getElementsByClassName("close")[0];

// Créer la fonction qui permet d'ouvrir le modal au click
btn.onclick = function() {
  modal.style.display = "block";
}

// Créer la fonction qui permet de fermer le modal
span.onclick = function() {
  modal.style.display = "none";
}

// Créer une fonction qui ferme le modal quand on click partout sur la page
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


// EXEMPLE HTML

<div class="infomodal">
  <!-- Lui demander de créer un bouton/click -->
  <button id="infobtn">A propos</button>

  <!-- Modal -->
  <div id="mymodal" class="modal">

  <!-- Modal Content -->
  <div class="modal_content">
    <span class="close">&times;</span>
    <p> Je m'appelle Anthony Sarapata, je suis un développeur junior
        en recherche d'aventure au sein d'une équipe.
        J'ai eu l'opportunité d'élargir mes connaissances et de
        développer des projets au sein de la nouvelle fabrique
        PopSchool, je vous laisse les découvrirs.</p>
  </div>
<!-- Mettre un lien/image vers le portfolio aprés le "découvrirs" -->

/* MISE EN PAGE CSS DU MODAL */

/* Mise en page du modal en background */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgba(0,0,0,0.4);
}

/* Mise en forme de la boite du modal, le cadre */
.modal_content {
  color: #143B56;
  background-color: rgba(0,0,0,0.1);
  margin: auto;
  padding: 20px;
  border: 5px solid #143B56;
  width: 80%;
}

/* Mise en forme du qui ferme le modal */
.close {
  color: #143B56;
  float: right;
  font-size: 30px;
  font-weight: bolder;
}

.close:hover,
.close:focus {
  color: #143B56;
  text-decoration: none;
  cursor: wait;
}

/* FIN MISE EN PAGE MODAL */

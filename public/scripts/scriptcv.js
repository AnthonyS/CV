// Maj anniversaire //
function age (){
  var birthday = new Date("1992/12/09");
  var today = new Date();
  var years = today.getYear() - birthday.getYear();
  if (birthday.getMonth() > today.getMonth()) {
    years --;
  }
  if (birthday.getMonth()==today.getMonth() && birthday.getDay()>today.getDay()){
    years--;
  }

  return years
}
document.getElementById("age").innerHTML = age () + " ans";




// Test event on contactitems - modal //

var modal = document.getElementById('modal_hobbies');
document.getElementById('plane').addEventListener('mouseover', function(){
  document.getElementById('modal_hobbies').style.display = "block";
  document.getElementById('text_hobbies').innerHTML = "<p> La découverte du monde permet de se découvrir soi-même. </p>"
})
document.getElementById('plane').addEventListener('mouseout', function(){
  document.getElementById('modal_hobbies').style.display = "none";
})


document.getElementById('movies').addEventListener('mouseover', function(){
  document.getElementById('modal_hobbies').style.display = "block";
  document.getElementById('text_hobbies').innerHTML ="<p> Le cinéma offre un épanouïssement, qu'il soit culturel ou non. </p>"
})
document.getElementById('movies').addEventListener('mouseout', function(){
  document.getElementById('modal_hobbies').style.display = "none";
})


document.getElementById('book').addEventListener('mouseover', function(){
  document.getElementById('modal_hobbies').style.display = "block";
  document.getElementById('text_hobbies').innerHTML = "<p> La lecture a les bienfaits et la vertu nécessaire pour le développement personnel. </p>"
})
document.getElementById('book').addEventListener('mouseout', function(){
  document.getElementById('modal_hobbies').style.display = "none";
})

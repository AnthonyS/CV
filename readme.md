Curriculum vitae
Auteur et copyright : Anthony Sarapata
État du projet : En cours a la date du 31/07/18

Description :
Création d'un Cv à l'aide des languages -- HTML, CSS, JS --


Rapport de bug :
-affichage du modal, léger bug d'affichage -affichage au dessous plutôt qu'en haut
-footer trop petit


Nouveau cv dans "newindex.html"
-contenu principal ok
-barre de navigation (couleur, forme, etc) ok
-couleur et police du site ok, leto style à télécharger
-timeline correct

En cours :
-Création d'un arriére plan en couleur comme la bar de réseaux et graphique blanc
-Logo graphique en position fixe et les titres des div apparait en animation
-Essai du responsive
-Mise en place animations css (div photo_info, timeline)
-Timeline horizontale sur compétences + responsive








Resume
Writer and copyright: Anthony Sarapata
State of the project: In progress as of 31/07/18

Description:
Creation of a CV thanks to langages -- HTML, CSS, JS --

Bug report:
-display of the modal, slight bug of display -> down rather than up
-too small footer


New resume in "newindex.html"
-main content is ok
-navbar (color, form, etc) ok
-color and police ok, dl leto styles
-timeline correct

In progress :
-Create a color background like the network bar and white graph
-Graph logo position fixed and title div appaers (animated)
-Try responsive
-Make animation css (div photo_info, timeline)
-Timeline horizontal on skills_language + responsive
